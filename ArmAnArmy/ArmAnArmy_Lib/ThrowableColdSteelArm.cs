﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArmAnArmy_Lib
{
	public class ThrowableColdSteelArm : AbstractArms
	{
		private bool _hasGrip;
		private string _typeOfDamage;

		public bool HasGrip
		{
			get { return _hasGrip; }
			set { _hasGrip = value; }
		}
		public string TypeOfDamage
		{
			get { return _typeOfDamage; }
			set { if (value != "") _typeOfDamage = value; else throw new ArgumentNullException("Damage type", "This characteristic can't be empty."); }
		}

		public override void SetUnitCost()
		{
			double mult = 1.0;
			if (_hasGrip)
			{
				mult += 0.2;
			}
			switch (_typeOfDamage)
			{
				case "Impact":
					mult += 0.0;
					break;
				case "Puncture":
					mult += 0.33;
					break;
				case "Slash":
					mult += 0.25;
					break;
			}

			_unitCost = Math.Round(_costModifier * mult * 1000.0, 2);
		}
	}
}

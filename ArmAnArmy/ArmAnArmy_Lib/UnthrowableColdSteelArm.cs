﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArmAnArmy_Lib
{
	public class UnthrowableColdSteelArm : AbstractArms
	{
		protected string _typeOfBlade;
		protected string _typeOfDamage;
		protected string _typeOfWield;
		
		public string TypeOfBlade
		{
			get { return _typeOfBlade; }
			set { if (value != "") _typeOfBlade = value; else throw new ArgumentNullException("Blade type", "This characteristic can't be empty."); }
		}
		public string TypeOfDamage
		{
			get { return _typeOfDamage; }
			set { if (value != "") _typeOfDamage = value; else throw new ArgumentNullException("Damage type", "This characteristic can't be empty."); }
		}
		public string TypeOfWield
		{
			get { return _typeOfWield; }
			set { if (value != "") _typeOfWield = value; else throw new ArgumentNullException("Holding type", "This characteristic can't be empty."); }
		}

		public override void SetUnitCost()
		{
			double mult = 1.0;
			switch (_typeOfBlade)
			{
				case "Single-edged":
					mult += 0.16;
					break;
				case "Double-edged":
					mult += 0.32;
					break;
				case "Multi-edged":
					mult += 0.64;
					break;
			}
			switch (_typeOfDamage)
			{
				case "Impact":
					mult += 0.0;
					break;
				case "Puncture":
					mult += 0.33;
					break;
				case "Slash":
					mult += 0.25;
					break;
			}
			switch (_typeOfWield)
			{
				case "One-handed":
					mult += 0.0;
					break;
				case "Two-handed":
					mult += 0.12;
					break;
				case "Variable":
					mult += 0.34;
					break;
			}

			_unitCost = Math.Round(_costModifier * mult * 1000.0, 2);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArmAnArmy_Lib
{
	public class Army
	{
		private string _name;
		private int _numberOfSoldiers;
		private List<AbstractArms> _armsList = new List<AbstractArms>();

		public string Name
		{
			get { return _name; }
			set { if (value != "") _name = value; else throw new ArgumentNullException("Name", "This characteristic can't be empty."); }
		}
		public int NumberOfSoldiers
		{
			get { return _numberOfSoldiers; }
			set { if (value > 0) _numberOfSoldiers = value; else throw new ArgumentOutOfRangeException("Number of soldiers", value, "Army must have at least 1 soldier."); }
		}
		public void AddArmToList(AbstractArms armToAdd)
		{
			_armsList.Add(armToAdd);
		}
		public void RemoveArmToList(AbstractArms armToRemove)
		{
			_armsList.Remove(armToRemove);
		}

		/*public double CalculateTotalArmsCost()
		{
			double sum = 0.0;
			foreach (AbstractArms arm in _armsList)
			{
				sum += arm.UnitCost;
			}
			return sum;
		}*/

		public List<AbstractArms> GetArms()
		{
			return _armsList;
		}
	}
}

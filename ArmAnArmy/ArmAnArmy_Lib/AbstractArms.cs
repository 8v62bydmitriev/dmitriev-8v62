﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArmAnArmy_Lib
{
	public abstract class AbstractArms
	{
		protected string _name;
		protected double _unitCost;
		protected double _costModifier;
		//protected double _unitWeight;

		public string Name
		{
			get { return _name; }
			set { if (value != "") _name = value; else throw new ArgumentNullException("Name", "This characteristic can't be empty."); }
		}
		public double UnitCost
		{
			get { return _unitCost; }
			set { _costModifier = value; SetUnitCost(); }
		}
		public double CostModifier
		{
			get { return _costModifier; }
		}
		/*public double UnitWeight
		{
			get { return _unitWeight; }
			set { if (value > 0.0) _unitWeight = value; else throw new ArgumentOutOfRangeException("Unit weight", value, "Unit weight can't be negative or zero number."); }
		}*/

		public abstract void SetUnitCost();
	}
}

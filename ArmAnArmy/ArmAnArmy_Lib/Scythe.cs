﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArmAnArmy_Lib
{
	class Scythe : UnthrowableColdSteelArm
	{
		private bool _isBelongingToDeath;
		private List<string> _classesCompatibleList;

		public bool IsBelongingToDeath
		{
			get { return _isBelongingToDeath; }
			set { _isBelongingToDeath = value; }
		}
		public List<string> ClassesCompatibleList
		{
			get { return _classesCompatibleList; }
			set { if (value.Count > 0) _classesCompatibleList = value; else throw new ArgumentOutOfRangeException("List of compatible classes", value, "Scythe must be compatible with at least 1 playable class."); }
		}

		public void HarvestSomeSouls()
		{
			if (IsBelongingToDeath)
			{
				throw new NotImplementedException("There aren't any souls to harvest.");
			}
			else
			{
				throw new ArgumentException("Only Death can harvest souls with its scythe.", "Belongs to Death");
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArmAnArmy_Lib
{
	public class Firearm : AbstractArms
	{
		private int _feedSystemCapacity;
		private double _distanceEffective;
		private double _rateOfFire;

		public int FeedSystemCapacity
		{
			get { return _feedSystemCapacity; }
			set { if (value > 0) _feedSystemCapacity = value; else throw new ArgumentOutOfRangeException("Feed system capacity", value, "Feed system capacity can't be negative or zero number."); }
		}
		public double DistanceEffective
		{
			get { return _distanceEffective; }
			set { if (value >= 0.0) _distanceEffective = value; else throw new ArgumentOutOfRangeException("Effective distance", value, "Distance can't be negative number."); }
		}
		public double RateOfFire
		{
			get { return _rateOfFire; }
			set { if (value > 0.0) _rateOfFire = value; else throw new ArgumentOutOfRangeException("Rate of fire", value, "Rate of fire can't be negative or zero number."); }
		}

		public override void SetUnitCost()
		{
			_unitCost = Math.Round(_costModifier * _feedSystemCapacity * _distanceEffective * _rateOfFire / 10000.0, 2);
		}
	}
}

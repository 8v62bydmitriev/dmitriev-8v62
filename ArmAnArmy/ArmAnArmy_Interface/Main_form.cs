﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ArmAnArmy_Lib;

namespace ArmAnArmy_Interface
{
	public partial class Main_form : Form
	{
		List<AbstractArms> ArmsList;

		public Main_form()
		{
			InitializeComponent();
			ArmsList = new List<AbstractArms>();
			Price_lab.Text = String.Format("Total price: {0:C}", 0);
		}

		private void Add_but_Click(object sender, EventArgs e)
		{
			Arm_form addForm = new Arm_form();
			addForm.ShowDialog();
			if (addForm.DialogResult == DialogResult.OK)
			{
				ArmsList.Add(addForm.GetArm());
				RefreshArms();
			}
			Arms_LB.Focus();
		}

		private void Edit_but_Click(object sender, EventArgs e)
		{
			if (Arms_LB.SelectedIndex != -1)
			{
				Arm_form addForm = new Arm_form(ArmsList[Arms_LB.SelectedIndex]);
				addForm.ShowDialog();
				if (addForm.DialogResult == DialogResult.OK)
				{
					ArmsList[Arms_LB.SelectedIndex] = addForm.GetArm();
					RefreshArms();
				}
				Arms_LB.Focus();
			}
		}

		private void Delete_but_Click(object sender, EventArgs e)
		{
			int id = Arms_LB.SelectedIndex;
			if (id != -1)
			{
				ArmsList.RemoveAt(id);
				RefreshArms();
				if (id > 0)
				{
					Arms_LB.SelectedIndex = id - 1;
					Arms_LB.Focus();
				}
				else if (Arms_LB.Items.Count > 0)
				{
					Arms_LB.SelectedIndex = id;
					Arms_LB.Focus();
				}
				else
				{
					Add_but.Focus();
				}
			}
		}

		private void RefreshArms()
		{
			double dollarRate = 63.4115409;
			double totalPrice = 0;
			Arms_LB.Items.Clear();
			for (int i = 0; i < ArmsList.Count; i++)
			{
				AbstractArms arm = ArmsList[i];
				Arms_LB.Items.Add(String.Format(i + 1 + ") " + arm.Name + " : {0:C}", arm.UnitCost * dollarRate));
				totalPrice += arm.UnitCost * dollarRate;
			}
			Price_lab.Text = String.Format("Total price: {0:C}", totalPrice);
		}
	}
}

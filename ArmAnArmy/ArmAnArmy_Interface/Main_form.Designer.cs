﻿namespace ArmAnArmy_Interface
{
	partial class Main_form
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			this.Arms_LB = new System.Windows.Forms.ListBox();
			this.Add_but = new System.Windows.Forms.Button();
			this.Edit_but = new System.Windows.Forms.Button();
			this.Delete_but = new System.Windows.Forms.Button();
			this.Price_lab = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Arms_LB
			// 
			this.Arms_LB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Arms_LB.FormattingEnabled = true;
			this.Arms_LB.ItemHeight = 15;
			this.Arms_LB.Location = new System.Drawing.Point(12, 12);
			this.Arms_LB.Name = "Arms_LB";
			this.Arms_LB.Size = new System.Drawing.Size(490, 379);
			this.Arms_LB.TabIndex = 20;
			// 
			// Add_but
			// 
			this.Add_but.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Add_but.Location = new System.Drawing.Point(515, 12);
			this.Add_but.Margin = new System.Windows.Forms.Padding(10, 3, 10, 10);
			this.Add_but.Name = "Add_but";
			this.Add_but.Size = new System.Drawing.Size(90, 45);
			this.Add_but.TabIndex = 5;
			this.Add_but.Text = "Add a weapon";
			this.Add_but.UseVisualStyleBackColor = true;
			this.Add_but.Click += new System.EventHandler(this.Add_but_Click);
			// 
			// Edit_but
			// 
			this.Edit_but.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Edit_but.Location = new System.Drawing.Point(515, 70);
			this.Edit_but.Margin = new System.Windows.Forms.Padding(10, 3, 10, 10);
			this.Edit_but.Name = "Edit_but";
			this.Edit_but.Size = new System.Drawing.Size(90, 45);
			this.Edit_but.TabIndex = 10;
			this.Edit_but.Text = "Edit the weapon";
			this.Edit_but.UseVisualStyleBackColor = true;
			this.Edit_but.Click += new System.EventHandler(this.Edit_but_Click);
			// 
			// Delete_but
			// 
			this.Delete_but.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Delete_but.Location = new System.Drawing.Point(515, 128);
			this.Delete_but.Margin = new System.Windows.Forms.Padding(10, 3, 10, 10);
			this.Delete_but.Name = "Delete_but";
			this.Delete_but.Size = new System.Drawing.Size(90, 45);
			this.Delete_but.TabIndex = 15;
			this.Delete_but.Text = "Delete the weapon";
			this.Delete_but.UseVisualStyleBackColor = true;
			this.Delete_but.Click += new System.EventHandler(this.Delete_but_Click);
			// 
			// Price_lab
			// 
			this.Price_lab.AutoSize = true;
			this.Price_lab.Location = new System.Drawing.Point(12, 408);
			this.Price_lab.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
			this.Price_lab.Name = "Price_lab";
			this.Price_lab.Size = new System.Drawing.Size(98, 15);
			this.Price_lab.TabIndex = 21;
			this.Price_lab.Text = "Total price: ";
			// 
			// Main_form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(624, 442);
			this.Controls.Add(this.Price_lab);
			this.Controls.Add(this.Delete_but);
			this.Controls.Add(this.Edit_but);
			this.Controls.Add(this.Add_but);
			this.Controls.Add(this.Arms_LB);
			this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "Main_form";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Arm an Army";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox Arms_LB;
		private System.Windows.Forms.Button Add_but;
		private System.Windows.Forms.Button Edit_but;
		private System.Windows.Forms.Button Delete_but;
		private System.Windows.Forms.Label Price_lab;
	}
}


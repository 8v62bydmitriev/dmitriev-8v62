﻿namespace ArmAnArmy_Interface
{
	partial class Arm_form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Class_CB = new System.Windows.Forms.ComboBox();
			this.Class_lab = new System.Windows.Forms.Label();
			this.Name_lab = new System.Windows.Forms.Label();
			this.Name_TB = new System.Windows.Forms.TextBox();
			this.Cost_lab = new System.Windows.Forms.Label();
			this.Cost_NUD = new System.Windows.Forms.NumericUpDown();
			this.FA_GB = new System.Windows.Forms.GroupBox();
			this.FA_ROF_TB = new System.Windows.Forms.TextBox();
			this.FA_ROF_lab = new System.Windows.Forms.Label();
			this.FA_ED_TB = new System.Windows.Forms.TextBox();
			this.FA_ED_lab = new System.Windows.Forms.Label();
			this.FA_FSC_TB = new System.Windows.Forms.TextBox();
			this.FA_FSC_lab = new System.Windows.Forms.Label();
			this.Accept_but = new System.Windows.Forms.Button();
			this.TCSA_GB = new System.Windows.Forms.GroupBox();
			this.TCSA_DT_CB = new System.Windows.Forms.ComboBox();
			this.TCSA_DT_lab = new System.Windows.Forms.Label();
			this.TCSA_HG_ChB = new System.Windows.Forms.CheckBox();
			this.UCSA_GB = new System.Windows.Forms.GroupBox();
			this.UCSA_WT_CB = new System.Windows.Forms.ComboBox();
			this.UCSA_BT_CB = new System.Windows.Forms.ComboBox();
			this.UCSA_DT_CB = new System.Windows.Forms.ComboBox();
			this.UCSA_WT_lab = new System.Windows.Forms.Label();
			this.UCSA_BT_lab = new System.Windows.Forms.Label();
			this.UCSA_DT_lab = new System.Windows.Forms.Label();
			this.Cancel_but = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.Cost_NUD)).BeginInit();
			this.FA_GB.SuspendLayout();
			this.TCSA_GB.SuspendLayout();
			this.UCSA_GB.SuspendLayout();
			this.SuspendLayout();
			// 
			// Class_CB
			// 
			this.Class_CB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Class_CB.FormattingEnabled = true;
			this.Class_CB.Items.AddRange(new object[] {
            "Firearm",
            "Throwable Cold Steel Arm",
            "Unthrowable Cold Steel Arm"});
			this.Class_CB.Location = new System.Drawing.Point(60, 12);
			this.Class_CB.Name = "Class_CB";
			this.Class_CB.Size = new System.Drawing.Size(262, 23);
			this.Class_CB.TabIndex = 5;
			this.Class_CB.SelectedIndexChanged += new System.EventHandler(this.Class_CB_SelectedIndexChanged);
			// 
			// Class_lab
			// 
			this.Class_lab.AutoSize = true;
			this.Class_lab.Location = new System.Drawing.Point(12, 15);
			this.Class_lab.Name = "Class_lab";
			this.Class_lab.Size = new System.Drawing.Size(42, 15);
			this.Class_lab.TabIndex = 4;
			this.Class_lab.Text = "Class";
			// 
			// Name_lab
			// 
			this.Name_lab.AutoSize = true;
			this.Name_lab.Location = new System.Drawing.Point(12, 44);
			this.Name_lab.Name = "Name_lab";
			this.Name_lab.Size = new System.Drawing.Size(35, 15);
			this.Name_lab.TabIndex = 9;
			this.Name_lab.Text = "Name";
			// 
			// Name_TB
			// 
			this.Name_TB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Name_TB.Location = new System.Drawing.Point(60, 41);
			this.Name_TB.Name = "Name_TB";
			this.Name_TB.Size = new System.Drawing.Size(262, 23);
			this.Name_TB.TabIndex = 10;
			// 
			// Cost_lab
			// 
			this.Cost_lab.AutoSize = true;
			this.Cost_lab.Location = new System.Drawing.Point(12, 73);
			this.Cost_lab.Name = "Cost_lab";
			this.Cost_lab.Size = new System.Drawing.Size(98, 15);
			this.Cost_lab.TabIndex = 14;
			this.Cost_lab.Text = "Cost modifier";
			// 
			// Cost_NUD
			// 
			this.Cost_NUD.DecimalPlaces = 2;
			this.Cost_NUD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.Cost_NUD.Location = new System.Drawing.Point(116, 71);
			this.Cost_NUD.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.Cost_NUD.Name = "Cost_NUD";
			this.Cost_NUD.Size = new System.Drawing.Size(55, 23);
			this.Cost_NUD.TabIndex = 15;
			this.Cost_NUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// FA_GB
			// 
			this.FA_GB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FA_GB.Controls.Add(this.FA_ROF_TB);
			this.FA_GB.Controls.Add(this.FA_ROF_lab);
			this.FA_GB.Controls.Add(this.FA_ED_TB);
			this.FA_GB.Controls.Add(this.FA_ED_lab);
			this.FA_GB.Controls.Add(this.FA_FSC_TB);
			this.FA_GB.Controls.Add(this.FA_FSC_lab);
			this.FA_GB.Location = new System.Drawing.Point(12, 97);
			this.FA_GB.Name = "FA_GB";
			this.FA_GB.Size = new System.Drawing.Size(310, 103);
			this.FA_GB.TabIndex = 18;
			this.FA_GB.TabStop = false;
			this.FA_GB.Visible = false;
			// 
			// FA_ROF_TB
			// 
			this.FA_ROF_TB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FA_ROF_TB.Location = new System.Drawing.Point(159, 74);
			this.FA_ROF_TB.Name = "FA_ROF_TB";
			this.FA_ROF_TB.Size = new System.Drawing.Size(145, 23);
			this.FA_ROF_TB.TabIndex = 15;
			this.FA_ROF_TB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Float_KeyPress);
			// 
			// FA_ROF_lab
			// 
			this.FA_ROF_lab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FA_ROF_lab.AutoSize = true;
			this.FA_ROF_lab.Location = new System.Drawing.Point(6, 77);
			this.FA_ROF_lab.Name = "FA_ROF_lab";
			this.FA_ROF_lab.Size = new System.Drawing.Size(91, 15);
			this.FA_ROF_lab.TabIndex = 14;
			this.FA_ROF_lab.Text = "Rate of fire";
			// 
			// FA_ED_TB
			// 
			this.FA_ED_TB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FA_ED_TB.Location = new System.Drawing.Point(159, 45);
			this.FA_ED_TB.Name = "FA_ED_TB";
			this.FA_ED_TB.Size = new System.Drawing.Size(145, 23);
			this.FA_ED_TB.TabIndex = 10;
			this.FA_ED_TB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Float_KeyPress);
			// 
			// FA_ED_lab
			// 
			this.FA_ED_lab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FA_ED_lab.AutoSize = true;
			this.FA_ED_lab.Location = new System.Drawing.Point(6, 48);
			this.FA_ED_lab.Name = "FA_ED_lab";
			this.FA_ED_lab.Size = new System.Drawing.Size(133, 15);
			this.FA_ED_lab.TabIndex = 9;
			this.FA_ED_lab.Text = "Effective distance";
			// 
			// FA_FSC_TB
			// 
			this.FA_FSC_TB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FA_FSC_TB.Location = new System.Drawing.Point(159, 16);
			this.FA_FSC_TB.Name = "FA_FSC_TB";
			this.FA_FSC_TB.Size = new System.Drawing.Size(145, 23);
			this.FA_FSC_TB.TabIndex = 5;
			this.FA_FSC_TB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Integer_KeyPress);
			// 
			// FA_FSC_lab
			// 
			this.FA_FSC_lab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FA_FSC_lab.AutoSize = true;
			this.FA_FSC_lab.Location = new System.Drawing.Point(6, 19);
			this.FA_FSC_lab.Name = "FA_FSC_lab";
			this.FA_FSC_lab.Size = new System.Drawing.Size(147, 15);
			this.FA_FSC_lab.TabIndex = 4;
			this.FA_FSC_lab.Text = "Feed system capacity";
			// 
			// Accept_but
			// 
			this.Accept_but.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Accept_but.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.Accept_but.Location = new System.Drawing.Point(177, 70);
			this.Accept_but.Name = "Accept_but";
			this.Accept_but.Size = new System.Drawing.Size(75, 30);
			this.Accept_but.TabIndex = 25;
			this.Accept_but.Text = "Accept";
			this.Accept_but.UseVisualStyleBackColor = true;
			this.Accept_but.Click += new System.EventHandler(this.Accept_but_Click);
			// 
			// TCSA_GB
			// 
			this.TCSA_GB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TCSA_GB.Controls.Add(this.TCSA_DT_CB);
			this.TCSA_GB.Controls.Add(this.TCSA_DT_lab);
			this.TCSA_GB.Controls.Add(this.TCSA_HG_ChB);
			this.TCSA_GB.Location = new System.Drawing.Point(12, 97);
			this.TCSA_GB.Name = "TCSA_GB";
			this.TCSA_GB.Size = new System.Drawing.Size(310, 103);
			this.TCSA_GB.TabIndex = 19;
			this.TCSA_GB.TabStop = false;
			this.TCSA_GB.Visible = false;
			// 
			// TCSA_DT_CB
			// 
			this.TCSA_DT_CB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TCSA_DT_CB.FormattingEnabled = true;
			this.TCSA_DT_CB.Items.AddRange(new object[] {
            "Impact",
            "Puncture",
            "Slash"});
			this.TCSA_DT_CB.Location = new System.Drawing.Point(96, 16);
			this.TCSA_DT_CB.Name = "TCSA_DT_CB";
			this.TCSA_DT_CB.Size = new System.Drawing.Size(208, 23);
			this.TCSA_DT_CB.TabIndex = 5;
			// 
			// TCSA_DT_lab
			// 
			this.TCSA_DT_lab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TCSA_DT_lab.AutoSize = true;
			this.TCSA_DT_lab.Location = new System.Drawing.Point(6, 19);
			this.TCSA_DT_lab.Name = "TCSA_DT_lab";
			this.TCSA_DT_lab.Size = new System.Drawing.Size(84, 15);
			this.TCSA_DT_lab.TabIndex = 4;
			this.TCSA_DT_lab.Text = "Damage type";
			// 
			// TCSA_HG_ChB
			// 
			this.TCSA_HG_ChB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TCSA_HG_ChB.AutoSize = true;
			this.TCSA_HG_ChB.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.TCSA_HG_ChB.Location = new System.Drawing.Point(6, 47);
			this.TCSA_HG_ChB.Name = "TCSA_HG_ChB";
			this.TCSA_HG_ChB.Size = new System.Drawing.Size(82, 19);
			this.TCSA_HG_ChB.TabIndex = 10;
			this.TCSA_HG_ChB.Text = "Has grip";
			this.TCSA_HG_ChB.UseVisualStyleBackColor = true;
			// 
			// UCSA_GB
			// 
			this.UCSA_GB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.UCSA_GB.Controls.Add(this.UCSA_WT_CB);
			this.UCSA_GB.Controls.Add(this.UCSA_BT_CB);
			this.UCSA_GB.Controls.Add(this.UCSA_DT_CB);
			this.UCSA_GB.Controls.Add(this.UCSA_WT_lab);
			this.UCSA_GB.Controls.Add(this.UCSA_BT_lab);
			this.UCSA_GB.Controls.Add(this.UCSA_DT_lab);
			this.UCSA_GB.Location = new System.Drawing.Point(12, 97);
			this.UCSA_GB.Name = "UCSA_GB";
			this.UCSA_GB.Size = new System.Drawing.Size(310, 103);
			this.UCSA_GB.TabIndex = 20;
			this.UCSA_GB.TabStop = false;
			this.UCSA_GB.Visible = false;
			// 
			// UCSA_WT_CB
			// 
			this.UCSA_WT_CB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.UCSA_WT_CB.FormattingEnabled = true;
			this.UCSA_WT_CB.Items.AddRange(new object[] {
            "One-handed",
            "Two-handed",
            "Variable"});
			this.UCSA_WT_CB.Location = new System.Drawing.Point(96, 74);
			this.UCSA_WT_CB.Name = "UCSA_WT_CB";
			this.UCSA_WT_CB.Size = new System.Drawing.Size(208, 23);
			this.UCSA_WT_CB.TabIndex = 15;
			// 
			// UCSA_BT_CB
			// 
			this.UCSA_BT_CB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.UCSA_BT_CB.FormattingEnabled = true;
			this.UCSA_BT_CB.Items.AddRange(new object[] {
            "Single-edged",
            "Double-edged",
            "Multi-edged"});
			this.UCSA_BT_CB.Location = new System.Drawing.Point(96, 45);
			this.UCSA_BT_CB.Name = "UCSA_BT_CB";
			this.UCSA_BT_CB.Size = new System.Drawing.Size(208, 23);
			this.UCSA_BT_CB.TabIndex = 10;
			// 
			// UCSA_DT_CB
			// 
			this.UCSA_DT_CB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.UCSA_DT_CB.FormattingEnabled = true;
			this.UCSA_DT_CB.Items.AddRange(new object[] {
            "Impact",
            "Puncture",
            "Slash"});
			this.UCSA_DT_CB.Location = new System.Drawing.Point(96, 16);
			this.UCSA_DT_CB.Name = "UCSA_DT_CB";
			this.UCSA_DT_CB.Size = new System.Drawing.Size(208, 23);
			this.UCSA_DT_CB.TabIndex = 5;
			// 
			// UCSA_WT_lab
			// 
			this.UCSA_WT_lab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.UCSA_WT_lab.AutoSize = true;
			this.UCSA_WT_lab.Location = new System.Drawing.Point(6, 77);
			this.UCSA_WT_lab.Name = "UCSA_WT_lab";
			this.UCSA_WT_lab.Size = new System.Drawing.Size(77, 15);
			this.UCSA_WT_lab.TabIndex = 14;
			this.UCSA_WT_lab.Text = "Wield type";
			// 
			// UCSA_BT_lab
			// 
			this.UCSA_BT_lab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.UCSA_BT_lab.AutoSize = true;
			this.UCSA_BT_lab.Location = new System.Drawing.Point(6, 48);
			this.UCSA_BT_lab.Name = "UCSA_BT_lab";
			this.UCSA_BT_lab.Size = new System.Drawing.Size(77, 15);
			this.UCSA_BT_lab.TabIndex = 9;
			this.UCSA_BT_lab.Text = "Blade type";
			// 
			// UCSA_DT_lab
			// 
			this.UCSA_DT_lab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.UCSA_DT_lab.AutoSize = true;
			this.UCSA_DT_lab.Location = new System.Drawing.Point(6, 19);
			this.UCSA_DT_lab.Name = "UCSA_DT_lab";
			this.UCSA_DT_lab.Size = new System.Drawing.Size(84, 15);
			this.UCSA_DT_lab.TabIndex = 4;
			this.UCSA_DT_lab.Text = "Damage type";
			// 
			// Cancel_but
			// 
			this.Cancel_but.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Cancel_but.Location = new System.Drawing.Point(265, 74);
			this.Cancel_but.Name = "Cancel_but";
			this.Cancel_but.Size = new System.Drawing.Size(57, 23);
			this.Cancel_but.TabIndex = 30;
			this.Cancel_but.Text = "Cancel";
			this.Cancel_but.UseVisualStyleBackColor = true;
			this.Cancel_but.Click += new System.EventHandler(this.Cancel_but_Click);
			// 
			// Arm_form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(334, 212);
			this.Controls.Add(this.Cancel_but);
			this.Controls.Add(this.Accept_but);
			this.Controls.Add(this.UCSA_GB);
			this.Controls.Add(this.TCSA_GB);
			this.Controls.Add(this.Cost_NUD);
			this.Controls.Add(this.Cost_lab);
			this.Controls.Add(this.Name_TB);
			this.Controls.Add(this.Name_lab);
			this.Controls.Add(this.Class_lab);
			this.Controls.Add(this.Class_CB);
			this.Controls.Add(this.FA_GB);
			this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "Arm_form";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Arm_form";
			((System.ComponentModel.ISupportInitialize)(this.Cost_NUD)).EndInit();
			this.FA_GB.ResumeLayout(false);
			this.FA_GB.PerformLayout();
			this.TCSA_GB.ResumeLayout(false);
			this.TCSA_GB.PerformLayout();
			this.UCSA_GB.ResumeLayout(false);
			this.UCSA_GB.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox Class_CB;
		private System.Windows.Forms.Label Class_lab;
		private System.Windows.Forms.Label Name_lab;
		private System.Windows.Forms.TextBox Name_TB;
		private System.Windows.Forms.Label Cost_lab;
		private System.Windows.Forms.NumericUpDown Cost_NUD;
		private System.Windows.Forms.GroupBox FA_GB;
		private System.Windows.Forms.Label FA_FSC_lab;
		private System.Windows.Forms.TextBox FA_FSC_TB;
		private System.Windows.Forms.TextBox FA_ROF_TB;
		private System.Windows.Forms.Label FA_ROF_lab;
		private System.Windows.Forms.TextBox FA_ED_TB;
		private System.Windows.Forms.Label FA_ED_lab;
		private System.Windows.Forms.Button Accept_but;
		private System.Windows.Forms.GroupBox TCSA_GB;
		private System.Windows.Forms.CheckBox TCSA_HG_ChB;
		private System.Windows.Forms.GroupBox UCSA_GB;
		private System.Windows.Forms.Label UCSA_WT_lab;
		private System.Windows.Forms.Label UCSA_BT_lab;
		private System.Windows.Forms.Label UCSA_DT_lab;
		private System.Windows.Forms.ComboBox UCSA_DT_CB;
		private System.Windows.Forms.ComboBox UCSA_BT_CB;
		private System.Windows.Forms.ComboBox UCSA_WT_CB;
		private System.Windows.Forms.ComboBox TCSA_DT_CB;
		private System.Windows.Forms.Label TCSA_DT_lab;
		private System.Windows.Forms.Button Cancel_but;
	}
}
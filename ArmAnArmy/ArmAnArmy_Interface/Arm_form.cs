﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ArmAnArmy_Lib;

namespace ArmAnArmy_Interface
{
	public partial class Arm_form : Form
	{
		private AbstractArms currentArm;

		public Arm_form()
		{
			InitializeComponent();
		}

		public Arm_form(AbstractArms armToEdit)
		{
			InitializeComponent();

			if (armToEdit is Firearm)
			{
				Class_CB.SelectedIndex = 0;
				Name_TB.Text = armToEdit.Name;
				Cost_NUD.Value = Convert.ToDecimal(armToEdit.CostModifier);
				FA_FSC_TB.Text = Convert.ToString((armToEdit as Firearm).FeedSystemCapacity);
				FA_ED_TB.Text = Convert.ToString((armToEdit as Firearm).DistanceEffective);
				FA_ROF_TB.Text = Convert.ToString((armToEdit as Firearm).RateOfFire);
			}
			else if (armToEdit is ThrowableColdSteelArm)
			{
				Class_CB.SelectedIndex = 1;
				Name_TB.Text = armToEdit.Name;
				Cost_NUD.Value = Convert.ToDecimal(armToEdit.CostModifier);
				switch ((armToEdit as ThrowableColdSteelArm).TypeOfDamage)
				{
					case "Impact":
						TCSA_DT_CB.SelectedIndex = 0;
						break;
					case "Puncture":
						TCSA_DT_CB.SelectedIndex = 1;
						break;
					case "Slash":
						TCSA_DT_CB.SelectedIndex = 2;
						break;
				}
				TCSA_HG_ChB.Checked = (armToEdit as ThrowableColdSteelArm).HasGrip;
			}
			else if (armToEdit is UnthrowableColdSteelArm)
			{
				Class_CB.SelectedIndex = 2;
				Name_TB.Text = armToEdit.Name;
				Cost_NUD.Value = Convert.ToDecimal(armToEdit.CostModifier);
				switch ((armToEdit as UnthrowableColdSteelArm).TypeOfDamage)
				{
					case "Impact":
						UCSA_DT_CB.SelectedIndex = 0;
						break;
					case "Puncture":
						UCSA_DT_CB.SelectedIndex = 1;
						break;
					case "Slash":
						UCSA_DT_CB.SelectedIndex = 2;
						break;
				}
				switch ((armToEdit as UnthrowableColdSteelArm).TypeOfBlade)
				{
					case "Single-edged":
						UCSA_BT_CB.SelectedIndex = 0;
						break;
					case "Double-edged":
						UCSA_BT_CB.SelectedIndex = 0;
						break;
					case "Multi-edged":
						UCSA_BT_CB.SelectedIndex = 0;
						break;
				}
				switch ((armToEdit as UnthrowableColdSteelArm).TypeOfWield)
				{
					case "One-handed":
						UCSA_WT_CB.SelectedIndex = 0;
						break;
					case "Two-handed":
						UCSA_WT_CB.SelectedIndex = 0;
						break;
					case "Variable":
						UCSA_WT_CB.SelectedIndex = 0;
						break;
				}
			}
		}

		private void Class_CB_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (Class_CB.SelectedIndex)
			{
				case 0:
					FA_GB.Visible = true;
					TCSA_GB.Visible = false;
					UCSA_GB.Visible = false;
					break;
				case 1:
					FA_GB.Visible = false;
					TCSA_GB.Visible = true;
					UCSA_GB.Visible = false;
					break;
				case 2:
					FA_GB.Visible = false;
					TCSA_GB.Visible = false;
					UCSA_GB.Visible = true;
					break;
			}
		}

		private void Accept_but_Click(object sender, EventArgs e)
		{
			bool fail = false;
			if ((Name_TB.Text == "") || (Class_CB.SelectedIndex == -1))
			{
				fail = true;
			}
			switch (Class_CB.SelectedIndex)
			{
				case 0:
					if ((FA_FSC_TB.Text == "") || (FA_ED_TB.Text == "") || (FA_ROF_TB.Text == "") || (Convert.ToInt32(FA_FSC_TB.Text)<=0) || (Convert.ToInt32(FA_ED_TB.Text) <= 0) || (Convert.ToInt32(FA_ROF_TB.Text) <= 0))
					{
						fail = true;
					}
					break;
				case 1:
					if (TCSA_DT_CB.SelectedItem.ToString() == "")
					{
						fail = true;
					}
					break;
				case 2:
					if ((UCSA_DT_CB.SelectedItem.ToString() == "") || (UCSA_BT_CB.SelectedItem.ToString() == "") || (UCSA_WT_CB.SelectedItem.ToString() == ""))
					{
						fail = true;
					}
					break;
			}

			if (fail)
			{
				MessageBox.Show("All fields must be filled correctly!", "Field filling error");
			}
			else
			{
				switch (Class_CB.SelectedIndex)
				{
					case 0:
						Firearm fArm = new Firearm();
						fArm.Name = Name_TB.Text;
						fArm.FeedSystemCapacity = Convert.ToInt32(FA_FSC_TB.Text);
						fArm.DistanceEffective = Convert.ToDouble(FA_ED_TB.Text);
						fArm.RateOfFire = Convert.ToDouble(FA_ROF_TB.Text);
						fArm.UnitCost = Convert.ToDouble(Cost_NUD.Value);
						currentArm = fArm;
						this.DialogResult = DialogResult.OK;
						break;
					case 1:
						ThrowableColdSteelArm tArm = new ThrowableColdSteelArm();
						tArm.Name = Name_TB.Text;
						tArm.TypeOfDamage = TCSA_DT_CB.SelectedItem.ToString();
						tArm.HasGrip = TCSA_HG_ChB.Checked;
						tArm.UnitCost = Convert.ToDouble(Cost_NUD.Value);
						currentArm = tArm;
						this.DialogResult = DialogResult.OK;
						break;
					case 2:
						UnthrowableColdSteelArm uArm = new UnthrowableColdSteelArm();
						uArm.Name = Name_TB.Text;
						uArm.TypeOfDamage = UCSA_DT_CB.SelectedItem.ToString();
						uArm.TypeOfBlade = UCSA_BT_CB.SelectedItem.ToString();
						uArm.TypeOfWield = UCSA_WT_CB.SelectedItem.ToString();
						uArm.UnitCost = Convert.ToDouble(Cost_NUD.Value);
						currentArm = uArm;
						this.DialogResult = DialogResult.OK;
						break;
				}
			}
		}

		private void Cancel_but_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}

		public AbstractArms GetArm()
		{
			return currentArm;
		}

		private void Integer_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!((e.KeyChar == (char)Keys.Back) || (e.KeyChar >= (char)Keys.D0) && (e.KeyChar <= (char)Keys.D9)))
			{
				e.Handled = true;
			}
		}

		private void Float_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!((e.KeyChar == (char)Keys.Back) || (e.KeyChar >= (char)Keys.D0) && (e.KeyChar <= (char)Keys.D9) || (e.KeyChar == ',')))
			{
				e.Handled = true;
			}

			if ((e.KeyChar == ',') && ((sender as TextBox).Text.Contains(",")))
			{
				e.Handled = true;
			}
		}
	}
}

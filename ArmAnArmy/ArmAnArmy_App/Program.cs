﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArmAnArmy_Lib;
using ArmAnArmy_Logic;

namespace ArmAnArmy_App
{
	class Program
	{
		static void Main(string[] args)
		{
			Army army = ArmyFactory.CreateArmy();

			ArmyPrinter printer = new ArmyPrinter();
			printer.Print(army);

			ArmyCalculator calc = new ArmyCalculator();
			double price = calc.GetPrice(army.GetArms());
			printer.Print(price);

			Console.ReadKey();
		}
	}
}

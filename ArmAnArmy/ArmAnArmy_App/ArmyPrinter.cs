﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArmAnArmy_Lib;

namespace ArmAnArmy_App
{
	public class ArmyPrinter
	{
		private double dollarRate = 63.4115409;

		public void Print(Army army)
		{
			Console.WriteLine("Army name: " + army.Name);
			Console.WriteLine("Number of soldiers: " + army.NumberOfSoldiers);
			Console.WriteLine(Environment.NewLine + "Weapons: " + Environment.NewLine);
			List<AbstractArms> arms = army.GetArms();
			for (int i = 0; i < arms.Count; i++)
			{
				AbstractArms wpn = arms[i];
				/*Console.Write("{0,10}", i + 1 + ") Name: ");
				Console.WriteLine(wpn.Name);
				Console.Write("{0,14}", "Cost: ");
				Console.WriteLine("{0:C}", wpn.UnitCost * dollarRate);*/

				Console.WriteLine("{0,10}" + wpn.Name + " Cost: " + "{1,15:C}", i + 1 + ") Name: ", wpn.UnitCost * dollarRate);
			}
		}

		public void Print(double price)
		{
			Console.WriteLine(Environment.NewLine + "{0,27} {1,15:C}", "Total price:", price * dollarRate);
		}
	}
}

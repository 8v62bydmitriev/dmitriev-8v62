﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArmAnArmy_Lib;

namespace ArmAnArmy_Logic
{
	public class ArmyCalculator
	{
		public double GetPrice(List<AbstractArms> arms)
		{
			double sum = 0.0;
			foreach (AbstractArms arm in arms)
			{
				sum += arm.UnitCost;
			}
			return sum;
		}
	}
}

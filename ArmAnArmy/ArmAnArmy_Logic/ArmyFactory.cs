﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArmAnArmy_Lib;

namespace ArmAnArmy_Logic
{
	public class ArmyFactory
	{
		static Random rnd = new Random();

		public static Army CreateArmy()
		{
			Army army = new Army();
			army.Name = Convert.ToChar(rnd.Next(65, 90 + 1)) + " Squad";
			army.NumberOfSoldiers = rnd.Next(1, 25 + 1);

			//int wpnNum = rnd.Next(1, army.NumberOfSoldiers + 1);
			for (int i = 0; i < army.NumberOfSoldiers; i++)
			{
				int wpnType = rnd.Next(1, 3 + 1);
				AbstractArms tempWpn = CreateArm(wpnType);
				army.AddArmToList(tempWpn);
			}

			/*Firearm testGun = new Firearm();
			testGun.Name = "AK-47";
			testGun.FeedSystemCapacity = 30;
			testGun.DistanceEffective = 200.0;
			testGun.RateOfFire = 300.0;
			testGun.UnitCost = 1.0;
			testGun.UnitWeight = 5.0;
			
			Army army = new Army();
			army.Name = "Delta Squad";
			army.NumberOfSoldiers = 4;
			army.AddArmToList(testGun);*/

			return army;
		}

		private static AbstractArms CreateArm(int armType)
		{
			switch (armType)
			{
				case 1:
					Firearm fArm = new Firearm();
					fArm.Name = "" + Convert.ToChar(rnd.Next(65, 90 + 1)) + Convert.ToChar(rnd.Next(65, 90 + 1)) + "-" + Convert.ToChar(rnd.Next(48, 57 + 1)) + Convert.ToChar(rnd.Next(48, 57 + 1)) + " (FA) ";
					fArm.FeedSystemCapacity = rnd.Next(10, 150 + 1);
					fArm.DistanceEffective = rnd.Next(1, 50000 + 1) / 10.0;
					fArm.RateOfFire = rnd.Next(60, 6000 + 1) / 2.0;
					fArm.UnitCost = rnd.Next(1, 10 + 1) / 10.0;
					//fArm.UnitWeight = rnd.Next(10, 100 + 1) / 10.0;
					return fArm;
				case 2:
					ThrowableColdSteelArm tArm = new ThrowableColdSteelArm();
					tArm.Name = "" + Convert.ToChar(rnd.Next(65, 90 + 1)) + Convert.ToChar(rnd.Next(48, 57 + 1)) + "-" + Convert.ToChar(rnd.Next(65, 90 + 1)) + " (TCSA)";
					tArm.HasGrip = Convert.ToBoolean(rnd.Next(0, 1 + 1));
					switch (rnd.Next(1, 3 + 1))
					{
						case 1:
							tArm.TypeOfDamage = "Impact";
							break;
						case 2:
							tArm.TypeOfDamage = "Puncture";
							break;
						case 3:
							tArm.TypeOfDamage = "Slash";
							break;
					}
					tArm.UnitCost = rnd.Next(1, 10 + 1) / 10.0;
					//tArm.UnitWeight = rnd.Next(5, 50 + 1) / 10.0;
					return tArm;
				case 3:
					UnthrowableColdSteelArm uArm = new UnthrowableColdSteelArm();
					uArm.Name = "" + Convert.ToChar(rnd.Next(65, 90 + 1)) + "-" + Convert.ToChar(rnd.Next(65, 90 + 1)) + Convert.ToChar(rnd.Next(48, 57 + 1)) + " (UCSA)";
					switch (rnd.Next(1, 3 + 1))
					{
						case 1:
							uArm.TypeOfBlade = "Single-edged";
							break;
						case 2:
							uArm.TypeOfBlade = "Double-edged";
							break;
						case 3:
							uArm.TypeOfBlade = "Multi-edged";
							break;
					}
					switch (rnd.Next(1, 3 + 1))
					{
						case 1:
							uArm.TypeOfDamage = "Impact";
							break;
						case 2:
							uArm.TypeOfDamage = "Puncture";
							break;
						case 3:
							uArm.TypeOfDamage = "Slash";
							break;
					}
					switch (rnd.Next(1, 3 + 1))
					{
						case 1:
							uArm.TypeOfWield = "One-handed";
							break;
						case 2:
							uArm.TypeOfWield = "Two-handed";
							break;
						case 3:
							uArm.TypeOfWield = "Variable";
							break;
					}
					uArm.UnitCost = rnd.Next(1, 10 + 1) / 10.0;
					//uArm.UnitWeight = rnd.Next(5, 100 + 1) / 10.0;
					return uArm;
				default:
					Firearm arm = new Firearm();
					arm.Name = "AK-47";
					arm.FeedSystemCapacity = 30;
					arm.DistanceEffective = 200.0;
					arm.RateOfFire = 300.0;
					arm.UnitCost = 1.0;
					//arm.UnitWeight = 5.0;
					return arm;
			}
		}
	}
}
